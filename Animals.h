#pragma once
#include <iostream>
class Animals
{
public:
	Animals() {};
	virtual void say() {};
	virtual ~Animals() {};
};

class Cow :public Animals
{
	void say() override
	{
		std::cout << "Cow said MOO" << std::endl;
	}
};

class Cat :public Animals
{
	void say() override
	{
		std::cout << "Cat said meow" << std::endl;
	}
};

class Dog :public Animals
{
	void say() override
	{
		std::cout << "Dog said auw" << std::endl;
	}
};

